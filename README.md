### ansible
This repo contains miscellaneous Ansible playbooks, brief explanations provided below on a best-effort basis.

##### _deploy_base_
Reusable Ansible roles intended to be used as a framework for my deployments

##### _build_kernel_
Automated kernel builds with ACS overrides patch for VFIO using Ansible and fedpkg.
