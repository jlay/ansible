## deploy_base
This playbook is used to test my misc. roles that I intend to use as a deployment framework.

### Known issues
The package updates for atomic aren't idempotent.  Some previous ostree transactions register a change, regardless of the actual transactional activity.

Simply comment out the update-packages role in play.yml to avoid this, if desired.

### Example usage
```
[jlay@delta deploy_base]$ ansible-playbook -i hosts play.yml
username for the user you want to create [jlay]:
password for created user:
confirm password for created user:

PLAY [qps] *********************************************************************

TASK [Gathering Facts] *********************************************************
ok: [node1]

TASK [bootstrap : check if atomic] *********************************************
ok: [node1]

TASK [bootstrap : check for cloud.cfg] *****************************************
ok: [node1]

TASK [bootstrap : set fact (atomic state)] *************************************
ok: [node1]

TASK [bootstrap : set fact (cloud.cfg state)] **********************************
ok: [node1]

TASK [bootstrap : remove update_etc_hosts from cloud.cfg] **********************
changed: [node1]

TASK [bootstrap : remove requiretty] *******************************************
ok: [node1]

TASK [bootstrap : install epel] ************************************************
skipping: [node1]

TASK [update-packages : update packages (atomic)] ******************************
changed: [node1]

TASK [update-packages : reboot updated hosts (atomic)] *************************
changed: [node1]

TASK [update-packages : wait for rebooted host to return (atomic)] *************
ok: [node1]

TASK [update-packages : update packages (non-atomic)] **************************
skipping: [node1]

TASK [update-packages : reboot updated hosts (non-atomic)] *********************
skipping: [node1]

TASK [update-packages : wait for rebooted hosts to return (non-atomic)] ********
skipping: [node1]

TASK [install-packages : install packages (Debian/Ubuntu)] *********************
skipping: [node1] => (item=bash-completion)
skipping: [node1] => (item=htop)
skipping: [node1] => (item=vim)
skipping: [node1] => (item=strace)
skipping: [node1] => (item=dmidecode)
skipping: [node1] => (item=dnsutils)

TASK [install-packages : install packages (RHEL/CentOS/Fedora)] ****************
skipping: [node1] => (item=bash-completion)
skipping: [node1] => (item=htop)
skipping: [node1] => (item=vim)
skipping: [node1] => (item=strace)
skipping: [node1] => (item=dmidecode)
skipping: [node1] => (item=iperf3)
skipping: [node1] => (item=nmap)
skipping: [node1] => (item=lvm2)

TASK [create-user : creating user jlay in wheel group (RHEL/CentOS/Fedora)] ****
changed: [node1]

TASK [create-user : creating user jlay in sudo group (Debian/Ubuntu)] **********
skipping: [node1]

TASK [create-user : copy current pubkey to ~jlay/.ssh/authorized_keys] *********
changed: [node1]

TASK [create-user : enable nopasswd sudo (RHEL/CentOS/Fedora)] *****************
changed: [node1]

TASK [create-user : enable nopasswd sudo (Debian/Ubuntu)] **********************
skipping: [node1]

TASK [hardening : disable password auth] ***************************************
changed: [node1]

TASK [docker : install docker] *************************************************
skipping: [node1]

TASK [docker : enable/start docker] ********************************************
skipping: [node1]

TASK [glusterfs-client : install glusterfs 4.1 LTS SIG package (CentOS)] *******
skipping: [node1]

TASK [glusterfs-client : install heketi-client (CentOS/Fedora)] ****************
skipping: [node1]

TASK [glusterfs-server : install glusterfs 4.1 LTS SIG package (CentOS)] *******
skipping: [node1]

TASK [glusterfs-server : install glusterfs-server] *****************************
skipping: [node1]

TASK [glusterfs-server : start glusterd] ***************************************
skipping: [node1]

TASK [glusterfs-server : add hosts to /etc/hosts] ******************************
changed: [node1] => (item=node1)

TASK [glusterfs-server : create glusterfs-server container (atomic)] ***********
changed: [node1]

RUNNING HANDLER [hardening : restart sshd] *************************************
changed: [node1]

PLAY RECAP *********************************************************************
node1                      : ok=17   changed=10   unreachable=0    failed=0

[jlay@delta deploy_base]$ 
```
